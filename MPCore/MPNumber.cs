﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Numerics;
using System.Text;

namespace MPCore
{
    public enum PrefixDigits
    {
        // error value
        NaMP = int.MinValue,

        // MP statuses
        DoomedOnZero = 0, // Contains a zero and can only do one step more
        DoomedOnFive = -5, // Contains a five and an even number and can only do two more steps.

        // MP values that go in front of the MP digits with their respective multiplication value
        ZeroZero = 1,
        ZeroTwo = 2,
        ZeroThree = 3,
        ZeroFour = 4,

        TwoThree = 6,
        ThreeFour = 12,
    }

    public class MultiplesBuffer
    {
        public enum Digit
        {
            Five = 5,
            Seven = 7,
            Eight = 8,
            Nine = 9
        }

        private static MultiplesBuffer m_instance;

        private MultiplesBuffer()
        {
            MultiplesOfFive = new ConcurrentDictionary<uint, BigInteger>();
            MultiplesOfSeven = new ConcurrentDictionary<uint, BigInteger>();
            MultiplesOfEight = new ConcurrentDictionary<uint, BigInteger>();
            MultiplesOfNine = new ConcurrentDictionary<uint, BigInteger>();

            InitializeBuffers();
        }

        public static MultiplesBuffer Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new MultiplesBuffer();
                }

                return m_instance;
            }
        }

        private void InitializeBuffers()
        {
            MultiplesOfFive.TryAdd(0, new BigInteger(1));
            MultiplesOfFive.TryAdd(1, new BigInteger(5));

            MultiplesOfSeven.TryAdd(0, new BigInteger(1));
            MultiplesOfSeven.TryAdd(1, new BigInteger(7));

            MultiplesOfEight.TryAdd(0, new BigInteger(1));
            MultiplesOfEight.TryAdd(1, new BigInteger(8));

            MultiplesOfNine.TryAdd(0, new BigInteger(1));
            MultiplesOfNine.TryAdd(1, new BigInteger(9));
        }

        private readonly ConcurrentDictionary<uint, BigInteger> MultiplesOfFive;
        private readonly ConcurrentDictionary<uint, BigInteger> MultiplesOfSeven;
        private readonly ConcurrentDictionary<uint, BigInteger> MultiplesOfEight;
        private readonly ConcurrentDictionary<uint, BigInteger> MultiplesOfNine;

        public BigInteger GetProductOf(Digit digit, uint digitCount)
        {
            if (digitCount < 1)
            {
                return BigInteger.Zero;
            }

            BigInteger value;
            bool found = false;
            uint tempDigits = digitCount + 1;

            ConcurrentDictionary<uint, BigInteger> theDictionary = GetDictionary(digit);

            do
            {
                tempDigits--;
                found = theDictionary.TryGetValue(tempDigits, out value);
            } while (!found && tempDigits > 1);


            if (!found)
            {
                value = new BigInteger((int)digit);
                tempDigits = 1;
            }

            while (tempDigits < digitCount)
            {
                value *= new BigInteger((int)digit);
                tempDigits++;

                theDictionary[tempDigits] = value;
            }

            return value;
        }

        private ConcurrentDictionary<uint, BigInteger> GetDictionary(Digit digit)
        {
            switch (digit)
            {
                case Digit.Five: return MultiplesOfFive;
                case Digit.Seven: return MultiplesOfSeven;
                case Digit.Eight: return MultiplesOfEight;
                case Digit.Nine: return MultiplesOfNine;
                default: return null;
            }
        }
    }

    /// <summary>
    /// Represents a value in the chain of Multiplicative Persistence
    /// First, we need to know the digits making up this value. Due to the nature of MP, these can only be the predifined prefixes and an amount of 7's, 8's and 9's.
    /// Next, we calculate the multiplication of these digits. The result will be a BigInteger.
    /// Then we need to analyze this value 
    /// </summary>
    public class MPNumber
    {
        public readonly PrefixDigits Prefix;

        // Allthough fives don't make up a good MP, dealing with it in the chain is a bit of a pain... so keeping track of them after the first value
        public readonly uint Fives;

        // The real deal
        public readonly uint Sevens;
        public readonly uint Eights;
        public readonly uint Nines;

        private MPNumber(PrefixDigits prefix)
        {
            Prefix = prefix;
            Fives = 0;
            Sevens = 0;
            Eights = 0;
            Nines = 0;
        }

        public MPNumber(PrefixDigits prefix, uint fives, uint sevens, uint eights, uint nines)
        {
            Prefix = prefix;
            Fives = fives;
            Sevens = sevens;
            Eights = eights;
            Nines = nines;
        }

        public BigInteger GetMultiplicationValue()
        {
            if ((int)Prefix < 1)
                throw new Exception("Trying to calculate product on an invalid MPValue.");

            BigInteger value = new BigInteger((int)Prefix);

            if (Fives > 0) value *= MultiplesBuffer.Instance.GetProductOf(MultiplesBuffer.Digit.Five, Fives);
            if (Sevens > 0) value *= MultiplesBuffer.Instance.GetProductOf(MultiplesBuffer.Digit.Seven, Sevens);
            if (Eights > 0) value *= MultiplesBuffer.Instance.GetProductOf(MultiplesBuffer.Digit.Eight, Eights);
            if (Nines > 0) value *= MultiplesBuffer.Instance.GetProductOf(MultiplesBuffer.Digit.Nine, Nines);

            return value;
        }


        public static MPNumber ParseMP(ref BigInteger value)
        {
            uint[] digits = GetDigits(ref value);

            if (digits.Length == 0)
            {
                return new MPNumber(PrefixDigits.DoomedOnZero);
            }

            // If there's a 5 and an even value, MP at this point will be just one more than the current MP
            // maybe somehow do this while parsing the digits?
            if (digits[5] > 0 && (digits[2] > 0 || digits[4] > 0 || digits[6] > 0 || digits[8] > 0))
            {
                return new MPNumber(PrefixDigits.DoomedOnFive);
            }

            /* now reduce the digits to base values
             * Basically, we need the primes, but reworking it to 8's and 9's reduces the amount of digits to work with
             * 0's already reject the value
             * 1's can be ignored completely
             * 2's can be reduced to 8's and 4's
             * 4's can be reduced to 2's
             * 5's are a PITA
             * 6's can be reduced to 2's and 3's
            */

            // ones have no meaning for MP
#if DEBUG
            digits[1] = 0;
#endif
            // Six is a two and a three
            if (digits[6] > 0)
            {
                digits[2] += digits[6];
                digits[3] += digits[6];
#if DEBUG
                digits[6] = 0;
#endif
            }

            // two three's make a nine
            if (digits[3] > 1)
            {
                digits[9] += digits[3] / 2;
                digits[3] = digits[3] % 2;
            }

            // first, temporarely make all 4's into two's
            if (digits[4] > 0)
            {
                digits[2] += digits[4] * 2;
                digits[4] = 0;
            }

            // Now, three two's make an 8
            if (digits[2] > 2)
            {
                digits[8] += digits[2] / 3;
                digits[2] = digits[2] % 3;
            }

            // Two two's are a four again
            if (digits[2] == 2)
            {
                digits[4] = 1;
                digits[2] = 0;
            }

            Debug.Assert(digits[0] == 0);
            Debug.Assert(digits[1] == 0);
            Debug.Assert(digits[2] < 2);
            Debug.Assert(digits[3] < 2);
            Debug.Assert(digits[4] < 2);
            Debug.Assert(digits[2] + digits[4] < 2);
            Debug.Assert(digits[6] == 0);

            // now we just need to map these digits to the right prefix configuration
            PrefixDigits prefixDigits = PrefixDigits.NaMP;
            var prefixFlags = digits[2] + (digits[3] << 1) + (digits[4] << 2);
            switch (prefixFlags)
            {
                case 0:
                    prefixDigits = PrefixDigits.ZeroZero;
                    break;
                case 1:
                    prefixDigits = PrefixDigits.ZeroTwo;
                    break;
                case 2:
                    prefixDigits = PrefixDigits.ZeroThree;
                    break;
                case 3:
                    prefixDigits = PrefixDigits.TwoThree;
                    break;
                case 4:
                    prefixDigits = PrefixDigits.ZeroFour;
                    break;
                case 6:
                    prefixDigits = PrefixDigits.ThreeFour;
                    break;
                default:
                    throw new Exception("Prefix flag could not be determined due to an incorrect combination of 2's, 3's and 4's");
            }

            return new MPNumber(prefixDigits, digits[5], digits[7], digits[8], digits[9]);
        }

        public static uint[] GetDigits(ref BigInteger value)
        {
            uint[] integers = new uint[10];

            var sValue = value.ToString();

            foreach (var c in sValue)
            {
                int i = c - 48;

                if (i == 0)
                {
                    // as soon as one digit is zero, we can discard this whole calculation
                    return new uint[0];
                }

                if (i < 0 || i > 9)
                {
                    throw new Exception("Character in BigInteger string was invalid: i=" + i);
                }

                integers[i]++;
            }

            return integers;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            if (Prefix == PrefixDigits.ZeroZero)
                sb.Append("0");
            else if (Prefix == PrefixDigits.TwoThree)
                sb.Append("23");
            else if (Prefix == PrefixDigits.ThreeFour)
                sb.Append("34");
            else
                sb.Append((int)Prefix);

            sb.Append("_5^").Append(Fives);
            sb.Append("_7^").Append(Sevens);
            sb.Append("_8^").Append(Eights);
            sb.Append("_9^").Append(Nines);

            return sb.ToString();
        }

        public string ToFullString()
        {
            var sb = new StringBuilder();

            if (Prefix == PrefixDigits.ZeroZero)
            { }
            else if (Prefix == PrefixDigits.TwoThree)
                sb.Append("23");
            else if (Prefix == PrefixDigits.ThreeFour)
                sb.Append("34");
            else
                sb.Append((int)Prefix);

            for (var i = 0; i < Fives; i++)
                sb.Append("5");

            for (var i = 0; i < Sevens; i++)
                sb.Append("7");

            for (var i = 0; i < Eights; i++)
                sb.Append("8");

            for (var i = 0; i < Nines; i++)
                sb.Append("9");

            return sb.ToString();
        }

        public static IEnumerable<PrefixDigits> PossiblePrefixDigits()
        {
            yield return PrefixDigits.ZeroZero;
            yield return PrefixDigits.ZeroTwo;
            yield return PrefixDigits.ZeroThree;
            yield return PrefixDigits.ZeroFour;
            yield return PrefixDigits.TwoThree;
            yield return PrefixDigits.ThreeFour;
        }

        public static uint AmountOfPrefixDigits(PrefixDigits prefix)
        {
            switch (prefix)
            {
                case PrefixDigits.TwoThree:
                case PrefixDigits.ThreeFour:
                    return 2;
                case PrefixDigits.ZeroFour:
                case PrefixDigits.ZeroThree:
                case PrefixDigits.ZeroTwo:
                    return 1;
                default:
                    return 0;
            }
        }
    }
}
