﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Threading.Tasks;
using Mono.Options;

namespace MPCore
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			MainWithArguments(args);

			Console.WriteLine("Press any key to exit");
			Console.ReadKey();
		}

		private static void MainWithArguments(string[] args)
		{
			Console.WriteLine("Welcome to MPCore, the Multiplicative Persistence calculator. Temporary results will be written to " + WorkStore.resultPath + " while final results are stored in a file specified at the end of the run.");

			var myOptions = new Options();
			var options = new OptionSet
			{
				{"t|threads=", "The amount of simultations searchers to run. Default=2", (uint v) => myOptions.Threads = v},
				{"s|start=", "The amount of digits to start with. Default=10", (uint v) => myOptions.StartDigits = v},
				{"e|end=", "The amount of digits to stop at. Default=230", (uint v) => myOptions.EndDigits = v},
				{"o|timeout=", "The maximum duration in seconds for the process to run. Default=3600", (int v) => myOptions.TimeoutTime = new TimeSpan(0, 0, v)},
				{"L|logSignificance=", "The miminum amount of persistence for the number to get logged. Default=3", (int v) => myOptions.LogSignificance = v}
			};

			try
			{
				if (args.Length == 0)
				{
					Console.WriteLine("Please enter the arguments");
					options.WriteOptionDescriptions(Console.Out);

					args = Console.ReadLine().Split(' ');
				}

				// Parse the arguments, but ignore the ones we weren't expecting.
				var extra = options.Parse(args);
			}
			catch (OptionException e)
			{
				Console.Write("greet: ");
				Console.WriteLine(e.Message);
				Console.WriteLine("Try `greet --help' for more information.");
				return;
			}

			var store = new WorkStore(myOptions);

			try
			{
				store.DoWork();
			}
			catch (Exception e)
			{
				Console.WriteLine("An unexpected exception occurred: " + e.Message);
				Console.WriteLine(e.StackTrace);
			}

			Console.WriteLine("Store had {0} results", store.GetResultCount());
		}
	}

	public class Options
	{
		public uint EndDigits = 230;
		public int LogSignificance = 3;
		public uint StartDigits = 10;
		public uint Threads = 2;
		public TimeSpan TimeoutTime = new TimeSpan(1, 0, 0);
	}

	public class WorkStore
	{
		public const string resultPath = "Results.txt";
		private uint m_currentDigits;

		private DateTime m_endTime;
		private BigInteger m_maximumDigits;

		private readonly Options m_options;
		private Task[] m_threads;

		private readonly object m_workerLock = new object();
		private ConcurrentDictionary<string, int> Results;

		public WorkStore(Options options)
		{
			m_options = options;
		}

		public void DoWork()
		{
			Results = new ConcurrentDictionary<string, int>();

			if (File.Exists(resultPath))
				File.Delete(resultPath);

			m_endTime = DateTime.UtcNow + m_options.TimeoutTime;

			m_currentDigits = m_options.StartDigits;
			m_maximumDigits = m_options.EndDigits;

			m_threads = new Task[m_options.Threads];

			for (var i = 0; i < m_threads.Length; i++) m_threads[i] = Task.Factory.StartNew(Worker);

			Task.WaitAll(m_threads, m_options.TimeoutTime.Add(new TimeSpan(0, 0, 3)));

			string finalResultsFileName = string.Format("FinalResults_{1}-to-{2}LT{3}_{0:yyyyMMdd-HHmmss}.txt",
				DateTime.Now,
				m_options.StartDigits,
				m_options.EndDigits,
				m_options.LogSignificance);

			lock (m_workerLock)
			{
				using (var sw = new StreamWriter(finalResultsFileName))
				{
					sw.WriteLine("Analysis starting at {0} digits going to {1} digits with minimum significance of {2}", m_options.StartDigits, m_options.EndDigits,
						m_options.LogSignificance);

					foreach (var kvpResult in Results) sw.WriteLine("{0}:\t{1}", kvpResult.Value, kvpResult.Key);
				}
			}

			Console.WriteLine("Final results written to " + finalResultsFileName);
		}

		private void LogResult(MPNumber value, int MP)
		{
			var mpString = value.ToFullString();
			Console.WriteLine("\t\t{0:s}: MP({1}) for {2}", DateTime.UtcNow, MP, mpString);

			lock (m_workerLock)
			{
				using (var sw = new StreamWriter(resultPath, true))
				{
					sw.WriteLine("{0:s}: MP({1}) for {2}", DateTime.UtcNow, MP, mpString);
				}
			}
		}

		public int GetResultCount()
		{
			lock (m_workerLock)
			{
				return Results.Count;
			}
		}

		private Work GetWork()
		{
			var newWork = new Work();

			lock (m_workerLock)
			{
				if (m_currentDigits > m_maximumDigits || DateTime.UtcNow > m_endTime)
				{
					newWork.Completed = true;
				}
				else
				{
					newWork.AmountOfDigits = m_currentDigits;
					m_currentDigits++;
				}
			}

			Console.WriteLine("Distributed work order of {0} digits with {1:g} left", newWork.AmountOfDigits, m_endTime - DateTime.UtcNow);
			return newWork;
		}

		private void Worker()
		{
			var myWork = GetWork();
			while (!myWork.Completed)
			{
				HandleWork(myWork);
				myWork = GetWork();
			}
		}

		private void HandleWork(Work work)
		{
			var start = DateTime.UtcNow;
			var totalDigits = work.AmountOfDigits;
			var hits = 0;

			try
			{
				foreach (var prefixDigits in MPNumber.PossiblePrefixDigits())
				{
					var prefixCount = MPNumber.AmountOfPrefixDigits(prefixDigits);

					for (uint sevens = 0; sevens <= totalDigits - prefixCount; sevens++)
					for (uint eights = 0; eights <= totalDigits - prefixCount - sevens; eights++)
					{
						var nines = totalDigits - prefixCount - sevens - eights;

						if (nines > totalDigits)
							throw new Exception("Nines somehow became an invalid amount of digits.");

						var mpNumber = new MPNumber(prefixDigits, 0, sevens, eights, nines);

						var result = RecursiveMultiplicativePersistenceCheck(mpNumber);

						if (result >= m_options.LogSignificance)
						{
							Results.TryAdd(mpNumber.ToFullString(), result);
							hits++;
							LogResult(mpNumber, result);
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("An unexpected exception occurred on a worker thread: " + e.Message);
				Console.WriteLine(e.StackTrace);
			}

			var dur = DateTime.UtcNow - start;

			if (hits > 0)
				Console.WriteLine("Checking {0} digits took {1}\t\t\tfor {2} hits", work.AmountOfDigits, dur, hits);
			else
				Console.WriteLine("Checking {0} digits took {1}", work.AmountOfDigits, dur);
		}

		public static int RecursiveMultiplicativePersistenceCheck(MPNumber mpNumber)
		{
			if (mpNumber.Prefix == PrefixDigits.DoomedOnZero)
				return 1;

			if (mpNumber.Prefix == PrefixDigits.DoomedOnFive)
				return 2;

			var numberOfHighDigits = mpNumber.Fives + mpNumber.Sevens + mpNumber.Eights + mpNumber.Nines;
			if (numberOfHighDigits == 0)
				switch (mpNumber.Prefix)
				{
					case PrefixDigits.ThreeFour:
						// 34 goes to 12, goes to 2
						return 2;
					case PrefixDigits.TwoThree:
						// 23 goes to 6
						return 1;
					default:
						// No further MP possible
						return 0;
				}
			if (mpNumber.Prefix == PrefixDigits.ZeroZero && numberOfHighDigits == 1) return 0;

			var multiplicationResult = mpNumber.GetMultiplicationValue();
			var newMpNumber = MPNumber.ParseMP(ref multiplicationResult);

			// We made it this far, add one MP to the recursive result
			return RecursiveMultiplicativePersistenceCheck(newMpNumber) + 1;
		}

		private struct Work
		{
			public uint AmountOfDigits;
			public bool Completed;
		}
	}
}