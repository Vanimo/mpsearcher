# MPSearcher

A tool that scans for numbers meeting a certain Multiplicative Persistence level within the provided parameters.

## Usage
This is a console application and expects at least some arguments, although none are mandatory.

*  -t, --threads=VALUE          The amount of simultations searchers to run. Default=2
*  -s, --start=VALUE            The amount of digits to start with. Default=10
*  -e, --end=VALUE              The amount of digits to stop at. Default=230
*  -o, --timeout=VALUE          The maximum duration in seconds for the process to run. Default=3600
*  -L, --logSignificance=VALUE  The miminum amount of persistence for the number to get logged. Default=3

With these arguments, the specified amount of worker threads will be created and search for numbers matching the specified MP threshold.
Results are logged in Results.txt, while the final report is logged at the end with the exact name specified in the console.
Format: "FinalResults_{StartDigits}-to-{EndDigits}LT{LogSignificance}_{DateTime.Now:yyyyMMdd-HHmmss}.txt"

## Details
The work is currently separated into batches where each batch is a specific length of digits. Threads will request a batch and calculate the MP for any relevant combination of digits with the specified length.
The current performance gain of the program lies mostly with the storage of the multiplication result of any used amount of 7's, 8's and 9's. Which means for most cases, all we need to do is multiply the prefix, the 7's, 8's and 9's value.
