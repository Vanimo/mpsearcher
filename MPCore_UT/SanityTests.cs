using Microsoft.VisualStudio.TestTools.UnitTesting;
using MPCore;

namespace MPCore_UT
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void SanityCheck()
        {
            var mp = WorkStore.RecursiveMultiplicativePersistenceCheck(new MPNumber(PrefixDigits.ZeroFour, 1, 0, 0, 0));
            Assert.IsTrue(mp == 2);

            mp = WorkStore.RecursiveMultiplicativePersistenceCheck(new MPNumber(PrefixDigits.TwoThree, 0, 0, 0, 1));
            Assert.IsTrue(mp == 3);

            mp = WorkStore.RecursiveMultiplicativePersistenceCheck(new MPNumber(PrefixDigits.TwoThree, 0, 1, 1, 0));
            Assert.IsTrue(mp == 4);

            mp = WorkStore.RecursiveMultiplicativePersistenceCheck(new MPNumber(PrefixDigits.TwoThree, 0, 1, 2, 0));
            Assert.IsTrue(mp == 6);

            mp = WorkStore.RecursiveMultiplicativePersistenceCheck(new MPNumber(PrefixDigits.ZeroTwo, 0, 6, 6, 2));
            Assert.IsTrue(mp == 11);
        }
    }
}
