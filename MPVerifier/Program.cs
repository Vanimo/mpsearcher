﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MPVerifier
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("Enter value to verify");
                var value = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(value))
                    break;

                var bValue = BigInteger.Parse(value);
                var result = RecursiveMultiplicativePersistenceCheck(bValue);

                Console.WriteLine("MP is {0}", result);

            } while (true);
        }

        private static BigInteger MPThreshHold = new BigInteger(10);
        private const long MaxIntMultipliedDigits = long.MaxValue / 10;

        private static int RecursiveMultiplicativePersistenceCheck(BigInteger value)
        {
            if (value < MPThreshHold)
            {
                return 0;
            }

            var digits = value.ToString(CultureInfo.InvariantCulture);

            BigInteger newValue = BigInteger.One;
			long newValueHelper = 1;
            foreach (var d in digits)
            {
                newValueHelper *= (long)d - 48L;

                if (newValueHelper > MaxIntMultipliedDigits)
                {
                    newValue *= newValueHelper;
                    newValueHelper = 1;
                }
            }

            if (newValueHelper > 1)
                newValue *= newValueHelper;

            Console.WriteLine("\t" + newValue.ToString());

            return RecursiveMultiplicativePersistenceCheck(newValue) + 1;
        }
    }
}
